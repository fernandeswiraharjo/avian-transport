﻿/* Documentation
 * 001 nanda - 7 Nov 2016
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Collections;

namespace TransportService.pages
{
    public partial class UserManagement : System.Web.UI.Page
    {
        public string gBranchId, gUserId, gRoleId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        public bool getRole()
        {
            //get user accesssrole
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gRoleId = vUser.RoleID;
            string menuName = "User Management";
            var vRole = UserFacade.CheckUserLeverage(gRoleId, menuName);

            return vRole;
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        private void CheckBranchAccess()
        {
            if (CBAllAccess.Checked == true)
            {
                btnShowBranch.Enabled = false;
                btnCancelBranch.Enabled = false;
                lblValblBranch.Visible = false;

                foreach (ListItem item in blBranch.Items)
                {
                    item.Selected = false;
                }

                PanelBranch.Visible = false;
            }
            else
            {
                btnShowBranch.Enabled = true;
                btnCancelBranch.Enabled = true;
            }
        }

        private void BindData(string lUserID, string lBranchID, string lKeyword)
        {
            var listUser = new List<Core.Model.mdlUser>();

            if (getBranch() == "")
            {
                 listUser = UserFacade.GetUser(lKeyword, lBranchID);
            }
            else
            {
                 listUser = UserFacade.GetSearchUser(lKeyword, lBranchID);
            }

            var role = getRole();
            foreach (var user in listUser)
            {
                user.Role = role;
                btnNew.Visible = role;
            }
            
            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = listUser;
            pgitems.AllowPaging = true;
            pgitems.PageSize = 10;
            pgitems.CurrentPageIndex = PageNumber;
            if (pgitems.PageCount > 1)
            {
                rptPaging.Visible = true;
                ArrayList pages = new ArrayList();
                for (int i = 0; i < pgitems.PageCount; i++)
                    pages.Add((i + 1).ToString());
                rptPaging.DataSource = pages;
                rptPaging.DataBind();

                Control control;
                control = rptPaging.Items[PageNumber].FindControl("btnPage");
                if (control != null)
                {
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["class"] = "active";
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["ForeColor"] = "Black";
                }

                if (PageNumber == 0)
                {
                    lnkPrevious.Enabled = false;
                    lnkNext.Enabled = true;
                }
                else if (PageNumber == pgitems.PageCount - 1)
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = true;
                }
            }
            else
            {
                rptPaging.Visible = false;
                lnkPrevious.Visible = false;
                lnkNext.Visible = false;
            }
            RptUserlist.DataSource = pgitems;
            RptUserlist.DataBind();
        }

        private void ClearContent()
        {
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            if (gBranchId == "")
            {
                txtBranchID.Text = string.Empty;
                txtBranchID.ReadOnly = false;
                btnShowBranch.Enabled = true;
                btnSearchBranch.Enabled = true;
                txtSearchBranch.ReadOnly = false;
                btnCancelBranch.Enabled = true;
                CBAllAccess.Visible = true;
                //CBAllAccess.Enabled = true;
            }
            else
            {
                txtBranchID.Text = gBranchId;
                txtBranchID.ReadOnly = true;
                btnShowBranch.Enabled = true;
                btnSearchBranch.Enabled = false;
                txtSearchBranch.ReadOnly = true;
                btnCancelBranch.Enabled = true;
                CBAllAccess.Visible = false;
                //CBAllAccess.Enabled = false;
            }

            //txtBranchID.Text = "";
            txtPassword.Text = "";
            txtSearch.Text = "";
            txtSearchBranch.Text = "";
            txtUserID.Text = "";
            txtUsername.Text = "";
            txtUserID.ReadOnly = false;
            PanelBranch.Visible = false;
            PanelFormUser.Visible = false;
            lblValblBranch.Visible = false;

            btnInsert.Visible = true;
            btnUpdate.Visible = true;
        }

        public void Message(String msg)
        {
            string script = "window.onload = function(){ alert('";
            script += msg;
            script += "');";
            script += "window.location = '";
            script += "'; }";
            ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var listRole = UserFacade.GetddlRole();
                ddlRoleID.DataTextField = "RoleName";
                ddlRoleID.DataValueField = "RoleID";
                ddlRoleID.DataSource = listRole;
                ddlRoleID.DataBind();

                if (Session["User"] == null)
                {
                    Response.Redirect("Index.aspx");
                }
                else
                {
                    gUserId = Session["User"].ToString();
                    var vUser = UserFacade.GetUserbyID(gUserId);
                    gBranchId = vUser.BranchID;
                }


                BindData(gUserId, gBranchId, "");
                ClearContent();
                //CheckBranchAccess();
            }
        }

        protected void RptUserlist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                Control control;
                control = e.Item.FindControl("lnkUserID");
                if (control != null)
                {
                    string lid = ((LinkButton)control).Text;
                    //code untuk cek relation id yg terhubung
                    //////var vUser = UserFacade.GetUserbyID(lid);
                    //////if (vUser.RoleID.count == 0)
                    UserFacade.DeleteUser(lid);
                    //else
                    //{
                    //    Response.Write("<script>alert('Delete User Failed');</script>");
                    //    return;
                    //}

                    gUserId = Session["User"].ToString();
                    var vUser = UserFacade.GetUserbyID(gUserId);
                    gBranchId = vUser.BranchID;

                    BindData(gUserId, gBranchId, "");
                }
            }
            if (e.CommandName == "Link")
            {
                Control control;
                control = e.Item.FindControl("lnkUserID");

                if (control != null)
                {

                    txtUserID.Text = ((LinkButton)control).Text;
                    var vUser = UserFacade.GetUserbyID(txtUserID.Text);
                    txtUsername.Text = vUser.UserID;
                    txtPassword.Text = Core.Manager.CryptorEngine.Decode(vUser.Password);
                    ddlRoleID.SelectedValue = vUser.RoleID;
                    txtBranchID.Text = vUser.BranchID;
                    txtUserID.ReadOnly = true;

                    if (getBranch() == "")
                    {
                        CBAllAccess.Visible = true;

                        var cbBranch = BranchFacade.GetSearch(txtSearchBranch.Text, "");
                        blBranch.DataSource = cbBranch;
                        blBranch.DataTextField = "BranchName";
                        blBranch.DataValueField = "BranchID";
                        blBranch.DataBind();
                    }
                    else
                    {
                        CBAllAccess.Visible = false;

                        var cbBranch = BranchFacade.LoadSomeBranch(getBranch());
                        blBranch.DataSource = cbBranch;
                        blBranch.DataTextField = "BranchName";
                        blBranch.DataValueField = "BranchID";
                        blBranch.DataBind();
                    }

                    if (vUser.BranchID == "")
                    {
                        CBAllAccess.Checked = true;
                        btnShowBranch.Enabled = false;
                        btnCancelBranch.Enabled = false;
                        PanelBranch.Visible = false;
                    }
                    else
                    {
                        var SelectedBranch = BranchFacade.LoadSomeBranch(vUser.BranchID);
                        foreach (ListItem li in blBranch.Items)
                        {
                            if (SelectedBranch.Select(fld => fld.BranchID).Contains(li.Value))
                                li.Selected = true;
                            else
                                li.Selected = false;
                        }

                        PanelBranch.Visible = true;
                    }

                    btnInsert.Visible = false;
                    btnCancel.Visible = true;
                    btnUpdate.Visible = true;
                    PanelFormUser.Visible = true;
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> blBranchStrList = new List<string>();

            // Loop through each item.
            foreach (ListItem item in blBranch.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blBranchStrList.Add(item.Value);
                }
            }

            if (CBAllAccess.Checked == false)
            {
                if (blBranchStrList.Count == 0)
                {
                    lblValblBranch.Text = "*Required";
                    lblValblBranch.Visible = true;
                    PanelBranch.Visible = false;

                    return;
                }
                else
                {
                    lblValblBranch.Visible = false;
                }
            }
            else
            {
            }

            //Password Check
            if (txtPassword.Text == "")
            {
                //RequiredFieldValidator5.Enabled = false;
                //RequiredFieldValidator2.Enabled = false;

                UserFacade.UpdateUserWithoutPass(txtUserID.Text, txtUsername.Text, blBranchStrList, ddlRoleID.SelectedValue);
            }
            else
            {
                if (txtPassword.Text != txtRePassword.Text)
                {
                    lblvalPassword.Visible = true;
                    txtPassword.Text = "";
                    txtRePassword.Text = "";
                    txtPassword.Focus();
                    return;
                }

                string lEncrpass = Core.Manager.CryptorEngine.Encode(txtPassword.Text);
                UserFacade.UpdateUser(txtUserID.Text, txtUsername.Text, lEncrpass, blBranchStrList, ddlRoleID.SelectedValue);
            }
            
            //Response.Write("<script>alert('Success Update User');</script>");
            Message("Success Update User");

            ClearContent();
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> blBranchStrList = new List<string>();

            // Loop through each item.
            foreach (ListItem item in blBranch.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blBranchStrList.Add(item.Value);
                }
            }

            if (CBAllAccess.Checked == false)
            {
                if (blBranchStrList.Count == 0)
                {
                    lblValblBranch.Text = "*Required";
                    lblValblBranch.Visible = true;
                    PanelBranch.Visible = false;

                    return;
                }
                else
                {
                    lblValblBranch.Visible = false;
                }
            }
            else
            {
            }

            //Password Check
            if (txtPassword.Text != txtRePassword.Text)
            {
                lblvalPassword.Visible = true;
                txtPassword.Text = "";
                txtRePassword.Text = "";
                txtPassword.Focus();
                return;
            }

            string lEncrpass = Core.Manager.CryptorEngine.Encode(txtPassword.Text);
            var vUser = UserFacade.GetUserbyID(txtUserID.Text);
            if (vUser != null)
            {
                //Response.Write("<script>alert('ID already Exist');</script>");
                Message("ID already Exist");
                return;
            }
            UserFacade.InsertUser(txtUserID.Text, txtUsername.Text, lEncrpass , blBranchStrList, ddlRoleID.SelectedValue);
            //Response.Write("<script>alert('Success Insert User');</script>");
            Message("Success Insert User");

            ClearContent();

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            PanelFormUser.Visible = false;
            ClearContent();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            PageNumber = 0;
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            BindData("", gBranchId, txtSearch.Text);
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            ClearContent();
            btnInsert.Visible = true;
            btnUpdate.Visible = false;
            btnCancel.Visible = true;
            PanelFormUser.Visible = true;
        }

        protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            BindData(gUserId, getBranch() ,txtSearch.Text);
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;

            BindData(gUserId, getBranch(), txtSearch.Text);
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;
            BindData(gUserId, getBranch(), txtSearch.Text);
        }

        protected void btnShowBranch_Click(object sender, EventArgs e)
        {
            //BindDataBranch("");
            txtSearchBranch.Text = string.Empty;

            if (getBranch() == "")
            {
                blBranch.DataSource = BranchFacade.GetSearch(txtSearchBranch.Text, "");
                blBranch.DataTextField = "BranchName";
                blBranch.DataValueField = "BranchID";
                blBranch.DataBind();
            }
            else
            {
                blBranch.DataSource = BranchFacade.LoadSomeBranch(getBranch());
                blBranch.DataTextField = "BranchName";
                blBranch.DataValueField = "BranchID";
                blBranch.DataBind();
            }
             
            PanelBranch.Visible = true;
            txtSearchBranch.Text = string.Empty;
        }

        protected void btnCancelBranch_Click(object sender, EventArgs e)
        {       
            PanelBranch.Visible = false;
            txtSearchBranch.Text = string.Empty;
            txtBranchID.Text = string.Empty;
        }

        protected void btnSearchBranch_Click(object sender, EventArgs e)
        {
            BindDataBranch(txtSearchBranch.Text);
            PanelBranch.Visible = true;
        }

        private void BindDataBranch(string keyword)
        {
            blBranch.DataSource = BranchFacade.GetSearch(txtSearchBranch.Text , "");
            blBranch.DataTextField = "BranchName";
            blBranch.DataValueField = "BranchID";
            blBranch.DataBind();
        }

        protected void rptBranch_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Link")
            {
                Control control;
                control = e.Item.FindControl("lnkBranchId");
                if (control != null)
                {
                    string id;
                    id = ((LinkButton)control).Text;
                    txtBranchID.Text = id;
                    PanelBranch.Visible = false;
                }
            }
        }

        protected void CBAllAccess_CheckedChanged(object sender, EventArgs e)
        {
            CheckBranchAccess();
        }     
    }
}