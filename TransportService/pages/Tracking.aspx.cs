﻿/* documentation
 * 001 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Collections;

namespace TransportService.pages
{

    public partial class Tracking : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        private void ClearContent()
        {
                if (getBranch() == "")
                {
                    ddlSearchBranchID.DataSource = BranchFacade.LoadBranch2(getBranch());
                    ddlSearchBranchID.DataTextField = "BranchName";
                    ddlSearchBranchID.DataValueField = "BranchID";
                    ddlSearchBranchID.DataBind();
                    //txtBranchID.Text = string.Empty;
                    //txtBranchID.ReadOnly = false;
                }
                else
                {
                    ddlSearchBranchID.DataSource = BranchFacade.LoadSomeBranch(getBranch());
                    ddlSearchBranchID.DataTextField = "BranchName";
                    ddlSearchBranchID.DataValueField = "BranchID";
                    ddlSearchBranchID.DataBind();
                    //txtBranchID.Text = getBranch();
                    //txtBranchID.ReadOnly = true;
                }
                rptBranchCoordinate.DataSource = ddlSearchBranchID.DataSource;
                rptBranchCoordinate.DataBind();

                //txtBranchID.Text = string.Empty;
                txtDODate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                txtdate2.Text = DateTime.Now.ToString("MM/dd/yyyy");
                //PanelMap.Visible = false;
                Panel1.Visible = false;
                btnSelectAll.Visible = false;
                btnUnSelectAll.Visible = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ClearContent();
            }

        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> blEmployeeStrList = new List<string>();

            // Loop through each item.
            foreach (ListItem item in blEmployee.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blEmployeeStrList.Add(item.Value);
                }
            }

            if (blEmployeeStrList.Count == 0)
            {
                lblValblEmployee.Text = "*Required";
                lblValblEmployee.Visible = true;

                return;
            }
            else
            {
                lblValblEmployee.Visible = false;
            }
            

            var listVisitTracking = new List<Core.Model.mdlVisitTracking>();
            var listCustomerCoordinate = new List<Core.Model.mdlCustomer>();

            try
            {
                listVisitTracking.AddRange(VisitFacade.GetVisitTracking(ddlSearchBranchID.SelectedValue, Convert.ToDateTime(txtDODate.Text), Convert.ToDateTime(txtdate2.Text), blEmployeeStrList));
                listCustomerCoordinate.AddRange(CustomerFacade.GetCustomerorWarehouseCoordinate(listVisitTracking));
            }
            catch
            {
                //DateTime txtdateStart = DateTime.ParseExact(txtDODate.Text, "MM-dd-yyyy ", System.Globalization.CultureInfo.InvariantCulture);
                //DateTime txtdateEnd = DateTime.ParseExact(txtdate2.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                //listVisitTracking.AddRange(VisitFacade.GetVisitTracking(ddlSearchBranchID.SelectedValue, txtdateStart, txtdateEnd, blEmployeeStrList));
                //listCustomerCoordinate.AddRange(CustomerFacade.GetCustomerorWarehouseCoordinate(listVisitTracking));
                listVisitTracking = new List<Core.Model.mdlVisitTracking>();
            }

            if (listVisitTracking.Count == 0)
            {
                lblAlert.Text = "Location unavailable";
            }
            else
            {
                lblAlert.Text = "";
                rptVisitMarkers.DataSource = listVisitTracking;
                rptVisitMarkers.DataBind();
                rptCustomerMarkers.DataSource = listCustomerCoordinate;
                rptCustomerMarkers.DataBind();
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchBranchID(string prefixText, int count)
        {
            List<string> lBranchs = new List<string>();
            lBranchs = BranchFacade.AutoComplBranch(prefixText, count, "BranchID");
            return lBranchs;
        }

        protected void btnShowEmployee_Click(object sender, EventArgs e)
        {

            if (txtEmployeeID.Text == "")
            {
                blEmployee.DataSource = EmployeeFacade.LoadEmployeelistReport(ddlSearchBranchID.SelectedValue);
                blEmployee.DataTextField = "EmployeeName";
                blEmployee.DataValueField = "EmployeeID";
                blEmployee.DataBind();

            }
            else
            {
                blEmployee.DataSource = EmployeeFacade.LoadEmployeelistReport2(txtEmployeeID.Text,ddlSearchBranchID.SelectedValue);
                blEmployee.DataTextField = "EmployeeName";
                blEmployee.DataValueField = "EmployeeID";
                blEmployee.DataBind();
            }


            Panel1.Visible = true;
            btnSelectAll.Visible = true;
            btnUnSelectAll.Visible = true;
        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blEmployee.Items)
            {
                item.Selected = true;
            }
        }

        protected void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blEmployee.Items)
            {
                item.Selected = false;
            }
        }

    }


}