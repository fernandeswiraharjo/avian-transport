﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="Table.aspx.cs" Inherits="TransportService.pages.Table" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    MANAGE <small>DAILY MESSAGE </small>
                </h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
        <div class="clearfix">
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>
                            Table <small>DAILY MESSAGE</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a> </li>
                                    <li><a href="#">Settings 2</a> </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                        </ul>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="table-responsive">
                            <form class="form-horizontal form-label-left">
                            <div class="form-group">
                                <asp:Label ID="lbl1" runat="server" Text="Message ID or Name or Branch :" Width="210px"></asp:Label>
                                <asp:TextBox ID="txtshipFrom" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtshipFrom" ValidationGroup="SendDOXML" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:Label ID="lbl2" runat="server" Text="Valid Date :" Width="100px" Style="margin-left: 9px;"></asp:Label>
                                <asp:TextBox ID="txtSearch2" runat="server" Style="margin-left: -28px; margin-top: 0px"
                                    Width="150px" CssClass="tb5"></asp:TextBox>
                                <script type="text/javascript">
                                    var picker1 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=txtSearch2.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020]
                                                     //                                    setDefaultDate: true,
                                                     //                                    defaultDate : new Date()
                                                 }
                                                                          );
                                </script>
                            </div>
                            </form>
                            <table class="table table-striped jambo_table bulk_action">
                            <%--isi table --%>
                            </table>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
