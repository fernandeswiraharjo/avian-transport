﻿<%@ Page Title="Live Tracking" Language="C#" MasterPageFile="~/pages/Site.Master"
    AutoEventWireup="true" CodeBehind="LiveTracking.aspx.cs" Inherits="TransportService.pages.LiveTracking" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form Live Tracking</h3>
        </div>
        <%--<div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            Go!</button>
                    </span>
                </div>
            </div>
        </div>--%>
    </div>
    <div class="clearfix">
    </div>
    <asp:Panel ID="ParameterPanel" runat="server">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Filter By</h2>
                <%--<ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a> </li>
                            <li><a href="#">Settings 2</a> </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>--%>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <br />
                
                                    <div class="form-group">
                                        <p>
                                            <asp:ScriptManager ID="ScriptManager2" runat="server">
                                            </asp:ScriptManager>
                                            <asp:Label ID="lblBranchID" runat="server" Text="Branch ID" Width="100px" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px; margin-left:-10px"></asp:Label>
                                            <asp:Label ID="Label2" runat="server" Text=":" Width="10px" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px; margin-left:5px"></asp:Label>
                        <%--<asp:Label ID="lblBranchID" runat="server" Text="Branch ID" Width="100px"></asp:Label>
                                            <%--<asp:Label ID="lblBranchID" runat="server" Text="Branch ID" Width="100px"></asp:Label>
                                            <asp:Label ID="Label2" runat="server" Text=":" Width="10px"></asp:Label>--%>
                                            <%--<asp:TextBox ID="txtBranchID" runat="server"
                                                Width="150px" CssClass="tb5"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtBranchID" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            <ajaxToolkit:AutoCompleteExtender ID="txtBranchID_AutoCompleteExtender" runat="server"
                                                ServiceMethod="SearchBranchID" MinimumPrefixLength="2" EnableCaching="false"
                                                DelimiterCharacters="" Enabled="True" TargetControlID="txtBranchID">
                                            </ajaxToolkit:AutoCompleteExtender>--%>
                                            <asp:DropDownList ID="ddlSearchBranchID" runat="server" class="form-control" Style="margin-left:0px; width:150px">
                                        </asp:DropDownList>
                                        </p>
                                        <p>
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee ID" Width="100px"></asp:Label>
                                            <asp:Label ID="Label1" runat="server" Text=":" Width="10px"></asp:Label>
                                            <asp:TextBox ID="txtEmployee" runat="server" Width="150px" CssClass="tb5" PlaceHolder="Search Employee"></asp:TextBox>
                                            <%--001 nanda--%>
                                            <asp:Button ID="btnShowEmployee" runat="server" CssClass="btn btn-sm " Text="Show Employee"
                                                OnClick="btnShowEmployee_Click" />
                                            <%-- Validasi Checkbox--%>
                                            
                                            <asp:Panel ID="Panel1" CssClass="form-control" Height="200px" Width="400px" runat="server"
                                                ScrollBars="Vertical">
                                                <asp:CheckBoxList ID="blEmployee" runat="server">
                                                </asp:CheckBoxList>
                                                
                                            </asp:Panel>
                                            <asp:Button ID="btnSelectAll" runat="server" CssClass="btn btn-sm " Text="SELECT ALL"
                                                OnClick="btnSelectAll_Click" />
                                                <asp:Button ID="btnUnSelectAll" runat="server" CssClass="btn btn-sm " Text="UNSELECT ALL"
                                                OnClick="btnUnSelectAll_Click" />                     
                                        </p>
                                    </div>
                                </div>
            </div>
        </div>
            </asp:Panel>
                                                                    
        <div class="col-md-12 col-xs-12">
                <p>
                    <asp:Button ID="btnStart" runat="server" Text="Start Tracking" class="btn btn-sm btn-embossed btn-primary"
                        Font-Size="14px" OnClick="btnStart_Click" />
                    <asp:Button ID="btnEnd" runat="server" Text="End Tracking" class="btn btn-sm btn-embossed btn-primary"
                        Font-Size="14px" OnClick="btnEnd_Click" />
                </p>
                </div>

            <div class="col-md-12 col-xs-12">
            <%-- SHOW MAP--%>
            <asp:Label ID="lblAlert" runat="server" Text="" Style="text-align: center; color: #FF0000;
                font-weight: 700"></asp:Label>
                <asp:Timer ID="timer1" runat="server" Interval="50000" OnTick="timer1_tick">
                </asp:Timer>
            <asp:Panel ID="Panelmaps1" runat="server">
                <div id="dvMap" style="width: 100%; height: 640px">
                </div>
            </asp:Panel>
            <asp:Panel ID="Panelmaps2" runat="server">
                <div id="dvMap2" style="width: 100%; height: 640px">
                </div>
            </asp:Panel>
            </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
<link href="../js/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4a8ijWorZUkJKOtzheGKtV1RlyfDYW24&callback=initMap"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <%-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>--%>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../js/FormatNumber.js" type="text/javascript"></script>
    <script src="../js/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <%-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry,places"></script>--%>
    <!-- Support Jquery -->
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script src="../js/FormatNumber.js" type="text/javascript"></script>
    <script src="../js/common.js" type="text/javascript"></script>
    <script src="../js/MapDefault.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/GoogleMap.js"></script>
    <script src="../js/mapwithmarker.js" type="text/javascript"></script>
    <style type="text/css">
        .labels
        {
            color: black;
            background-color: #FF8075;
            font-family: Arial;
            font-size: 13px;
            font-weight: bold;
            text-align: center;
            width: 100px;
        }
    </style>
    <script type="text/javascript">
        var branchCoordinate = [
        <asp:Repeater ID="rptBranchCoordinate" runat="server">
        <ItemTemplate>
                    {
                    "lat": '<%# Eval("Latitude") %>',
                    "lng": '<%# Eval("Longitude") %>'
                }
        </ItemTemplate>
        <SeparatorTemplate>
            ,
        </SeparatorTemplate>
        </asp:Repeater>
        ];
</script>
    <script type="text/javascript">
        function loadMap() {
            //Create Map
            var objGoogleMapOption = {
                center: new google.maps.LatLng(branchCoordinate[0].lat, branchCoordinate[0].lng),
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            objMap = new google.maps.Map(document.getElementById("dvMap2"), objGoogleMapOption);
        }

        $(function () {
            loadMap();

        });
    </script>
    <script type="text/javascript">
        var markers = [
        <asp:Repeater ID="rptLiveTrackingMarkers" runat="server">
        <ItemTemplate>
                    {
                    "lTrackingDate": '<%# Eval("TrackingDate") %>',
                    "lat": '<%# Eval("Latitude") %>',
                    "lng": '<%# Eval("Longitude") %>',
                    "lEmployeeID": '<%# Eval("EmployeeID") %>',
                    "lBranchID": '<%# Eval("BranchID") %>',
                    "lVehicleID": '<%# Eval("VehicleID") %>',
                    "lStreetName": '<%# Eval("StreetName") %>'
                   

                }
        </ItemTemplate>
        <SeparatorTemplate>
            ,
        </SeparatorTemplate>
        </asp:Repeater>
        ];
    </script>
    <script type="text/javascript" id="EventScriptBlock">

        window.onload = function loadmaps() {

            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var infowindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);

            var lat_lng = new Array();
            for (i = 0; i < markers.length; i++) {
                var data = markers[i];
                var start = new google.maps.LatLng(data.lat, data.lng);
                var contentString = '<div id="content">' +
                            '<div id="siteNotice">' +
                            '</div>' +
                            '<h5 id="firstHeading" class="firstHeading">Employee ID : ' + data.lEmployeeID + ' | ' + data.lTrackingDate + '</h1><hr>' +
                            '<h5 id="secondHeading class="secondHeading">' + data.lStreetName + '</h3>' +
                            '<h5 id="secondHeading class="secondHeading">Vehicle ID : ' + data.lVehicleID + '</h3>' +
                            '<h5 id="secondHeading class="secondHeading">Branch ID : ' + data.lBranchID + '</h5>' +
                            '</div>';

                lat_lng.push(start);

                var marker1 = new MarkerWithLabel({
                    position: start,
                    map: map,
                    title: data.title,
                    labelContent: data.lEmployeeID,
                    labelAnchor: new google.maps.Point(15, 65),
                    labelClass: "labels", // the CSS class for the label
                    labelInBackground: false,
                    icon: "../Images/Visit.png"
                });
                google.maps.event.addListener(marker1, 'click', (function (marker1, contentString, infowindow) {
                    return function () {
                        infowindow.setContent(contentString);
                        infowindow.open(map, marker1);
                    };
                })(marker1, contentString, infowindow));


            }

            //            function fpoll() {
            //                //Run Every Set Period
            //                setInterval(loadmaps, 20000);
            //            }

            //            fpoll();
            //                        function fRefresh() {
            //                          
            //                            var mapOptions = {
            //                                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            //                                zoom: 10,
            //                                mapTypeId: google.maps.MapTypeId.ROADMAP
            //                            };

            //                            var infowindow = new google.maps.InfoWindow();
            //                            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);

            //                            var lat_lng = new Array();
            //                            for (i = 0; i < markers.length; i++) {
            //                                var data = markers[i];
            //                                var start = new google.maps.LatLng(data.lat, data.lng);
            //                                var contentString = data.description;

            //                                lat_lng.push(start);

            //                                var marker1 = new MarkerWithLabel({
            //                                    position: start,
            //                                    map: map,
            //                                    title: data.title,
            //                                    labelContent: i,
            //                                    labelAnchor: new google.maps.Point(7, 37),
            //                                    labelClass: "labels", // the CSS class for the label
            //                                    labelInBackground: false
            //                                });
            //                                google.maps.event.addListener(marker1, 'click', (function (marker1, contentString, infowindow) {
            //                                    return function () {
            //                                        infowindow.setContent(contentString);
            //                                        infowindow.open(map, marker1);
            //                                    };
            //                                })(marker1, contentString, infowindow)); 

            //                                 
            //                        }



            //                        function countdown() {
            //                            seconds = document.getElementById("timerLabel").innerHTML;
            //                            if (seconds > 0) {
            //                                document.getElementById("timerLabel").innerHTML = seconds - 1;
            //                                setTimeout("countdown()", 1000);
            //                                loadmaps;
            //                                loadmaps();
            //                            }
            //                        }

            //            setTimeout("countdown()", 1000);

            //            function poll() {
            //                setInterval(loadmaps, 20000);
            //            }

            // start the polling queue
            //            poll();

            //            function redraw() {
            //                var mapOptions = {
            //                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            //                zoom: 10,
            //                mapTypeId: google.maps.MapTypeId.ROADMAP
            //                };

            //                var infowindow = new google.maps.InfoWindow();
            //                var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);

            //                var lat_lng = new Array();
            //                for (i = 0; i < markers.length; i++) {
            //                    var data = markers[i];
            //                    var ldatalat = data.lat + 0.002;
            //                    var ldatalong = data.lng - 0.001;
            //                    var start = new google.maps.LatLng(ldatalat, ldatalong);
            //                    var contentString = data.description;

            //                    lat_lng.push(start);

            //                    var marker1 = new MarkerWithLabel({
            //                        position: start,
            //                        map: map,
            //                        title: data.title,
            //                        labelContent: i,
            //                        labelAnchor: new google.maps.Point(7, 30),
            //                        labelClass: "labels", // the CSS class for the label
            //                        labelInBackground: false
            //                    });
            //                    google.maps.event.addListener(marker1, 'click', (function (marker1, contentString, infowindow) {
            //                        return function () {
            //                            infowindow.setContent(contentString);
            //                            infowindow.open(map, marker1);
            //                        };
            //                    })(marker1, contentString, infowindow));
            //                     alert("Hello!");
            //            }

        }

        //        function myTimer() {
        //            var d = new Date();
        //            var t = d.toLocaleTimeString();
        //            document.getElementById("DivTime").innerHTML = t;
        //        }
            
    </script>
</asp:Content>
