﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using Core.Model;
using Newtonsoft.Json;


namespace TransportService.pages
{
    public partial class ReportIdleTracking : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (getBranch() == "")
                {
                    ddlSearchBranchID.DataSource = BranchFacade.LoadBranch2(getBranch());
                    ddlSearchBranchID.DataTextField = "BranchName";
                    ddlSearchBranchID.DataValueField = "BranchID";
                    ddlSearchBranchID.DataBind();
                    //txtBranchID.Text = string.Empty;
                    //txtBranchID.ReadOnly = false;
                }
                else
                {
                    ddlSearchBranchID.DataSource = BranchFacade.LoadSomeBranch(getBranch());
                    ddlSearchBranchID.DataTextField = "BranchName";
                    ddlSearchBranchID.DataValueField = "BranchID";
                    ddlSearchBranchID.DataBind();
                    //txtBranchID.Text = getBranch();
                    //txtBranchID.ReadOnly = true;
                }

                Panel1.Visible = false;
                txtDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                //btnSelectAll.Visible = false;
                //btnUnSelectAll.Visible = false;
            }
        }

        protected void btnShowEmployee_Click(object sender, EventArgs e)
        {
            //--002,004

            //if(txtBranchID.Text == "" && txtEmployeeID.Text == "")
            //{
            //    var lEmployeelist = EmployeeFacade.LoadEmployee();
            //    blEmployee.DataSource = lEmployeelist;
            //    blEmployee.DataTextField = "EmployeeName";
            //    blEmployee.DataValueField = "EmployeeID";
            //    blEmployee.DataBind();
            //}
            if (ddlSearchBranchID.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Please Fill Branch ID First');", true);
            }
            else if (txtEmployeeID.Text == "" && ddlSearchBranchID.SelectedValue != "")
            {
                var lEmployeelist = EmployeeFacade.LoadEmployeelistReport(ddlSearchBranchID.SelectedValue);
                rbEmployee.DataSource = lEmployeelist;
                rbEmployee.DataTextField = "EmployeeName";
                rbEmployee.DataValueField = "EmployeeID";
                rbEmployee.DataBind();

                Panel1.Visible = true;
                //btnSelectAll.Visible = true;
                //btnUnSelectAll.Visible = true;
            }
            else
            {
                var lEmployeelist = EmployeeFacade.LoadEmployeelistReport2(txtEmployeeID.Text, ddlSearchBranchID.SelectedValue);
                rbEmployee.DataSource = lEmployeelist;
                rbEmployee.DataTextField = "EmployeeName";
                rbEmployee.DataValueField = "EmployeeID";
                rbEmployee.DataBind();

                Panel1.Visible = true;
                //btnSelectAll.Visible = true;
                //btnUnSelectAll.Visible = true;
            }
            //002,004--

            //Panel1.Visible = true;
            //btnSelectAll.Visible = true;
            //btnUnSelectAll.Visible = true;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchBranchID(string prefixText, int count)
        {
            List<string> lBranchs = new List<string>();
            lBranchs = BranchFacade.AutoComplBranch(prefixText, count, "BranchID");
            return lBranchs;
        }

        public string json = "";
        public int rad = 0;

        protected void btnShow_Click(object sender, EventArgs e)
        {
            //rptTrackingMrk.DataSource = ReportJourneyTrackingFacade.GetTrackingCoordinate(rbEmployee.SelectedValue, txtBranchID.Text, Convert.ToDateTime(txtDate.Text));
            //rptTrackingMrk.DataBind();
            List<Core.Model.mdlSettings> listSettings = GeneralSettingsFacade.GetCurrentSettings(ddlSearchBranchID.SelectedValue);
            rad = Convert.ToInt32(listSettings.FirstOrDefault(fld => fld.name.Equals("IDLERADIUS")).value);

            try
            {
                var mdlListTrackingIdle = ReportIdleTrackingFacade.GetIdleTrackingList(rbEmployee.SelectedValue, ddlSearchBranchID.SelectedValue, Convert.ToDateTime(txtDate.Text), listSettings);
                
                json = JsonConvert.SerializeObject(mdlListTrackingIdle);
            }
            catch
            {
                DateTime txtDatee = DateTime.ParseExact(txtDate.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                var mdlListTrackingIdle = ReportIdleTrackingFacade.GetIdleTrackingList(rbEmployee.SelectedValue, ddlSearchBranchID.SelectedValue, txtDatee, listSettings);
                json = JsonConvert.SerializeObject(mdlListTrackingIdle);
            }

            System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //json = oSerializer.Serialize(mdlListTrackingIdle);

            //rptTrackingMrk.DataSource = mdlListTrackingIdle;
            //rptTrackingMrk.DataBind();
            

        }
    }
}