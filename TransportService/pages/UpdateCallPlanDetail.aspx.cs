﻿/* documentation
 *001 17 Okt 2016 fernandes
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Collections;

//namespace TransportService.pages
//{
    public partial class UpdateCallPlanDetail : System.Web.UI.Page
    {
        public string gBranchId, gUserId;

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        public string lParam { get; set; }

        private void BindData(string keywordDriver)
        {
            string callPlanDetailID = Request.QueryString["cid"];
            var callPlanDetail = CallPlanFacade.GetCallPlanByCPDetailID(callPlanDetailID);

            var listCallPlan = new List<Core.Model.CallPlan>();

            var callPlan = CallPlanFacade.GetCallPlan(callPlanDetail.CallPlanID);

            if (getBranch() == "")
            {
                listCallPlan = CallPlanFacade.GetCallPlanMove(keywordDriver, callPlan.CallPlanID, callPlan.Date.ToString("MM/dd/yyyy"), getBranch());
            }
            else
            {
                listCallPlan = CallPlanFacade.GetCallPlanMoveByBranch(keywordDriver, callPlan.CallPlanID, callPlan.Date.ToString("MM/dd/yyyy"), getBranch());
            }

            //rptProductList.DataSource = listProduct;
            //rptProductList.DataBind();

            //if (lParam == "search")
            //{
            //    listCallPlan = Core.Manager.CallPlanFacade.GetSearch(keywordDriver,keywordDate);
            //}
            //else if (lParam == "showall")
            //{
            //    listCallPlan = Core.Manager.CallPlanFacade.GetCallPlanAll();
            //}
            //else
            //{
            //    listCallPlan = Core.Manager.CallPlanFacade.GetCallPlan();
            //}

            PagedDataSource pgitems = new PagedDataSource();
            //DataView dv = new DataView(dt);
            pgitems.DataSource = listCallPlan;
            pgitems.AllowPaging = true;
            pgitems.PageSize = 10;
            pgitems.CurrentPageIndex = PageNumber;
            //lblPage.Text = pgitems.PageCount.ToString();
            if (pgitems.PageCount > 1)
            {
                lnkNext.Visible = true;
                lnkPrevious.Visible = true;
                rptPaging.Visible = true;

                ArrayList pages = new ArrayList();
                for (int i = 0; i < pgitems.PageCount; i++)
                    pages.Add((i + 1).ToString());
                rptPaging.DataSource = pages;
                rptPaging.DataBind();

                Control control;
                control = rptPaging.Items[PageNumber].FindControl("btnPage");
                if (control != null)
                {
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["class"] = "active";
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["ForeColor"] = "Black";
                }

                if (PageNumber == 0)
                {
                    lnkPrevious.Enabled = false;
                    lnkNext.Enabled = true;
                }
                else if (PageNumber == pgitems.PageCount - 1)
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = true;
                }
            }
            else
            {
                rptPaging.Visible = false;
                lnkPrevious.Visible = false;
                lnkNext.Visible = false;
            }

            RptCallPlanlist.DataSource = pgitems;
            RptCallPlanlist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //    if (Session["User"] == null)
                //    {
                //        Response.Redirect("home.aspx");
                //    }

                BindData("");

                //btnCancel.Attributes.Add("onClick", "javascript:history.back(); return false;");

            }
        }

        protected void rptCallPlan_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //if (e.CommandName == "Delete")
            //{
            //    Control control;
            //    control = e.Item.FindControl("lnkCallPlanId");
            //    if (control != null)
            //    {
            //        string id;
            //        id = ((LinkButton)control).Text;
            //        var callplan = CallPlanFacade.GetCallPlanDetail(id);
            //        if (callplan.StockOutDetails.Count == 0 && product.StockInDetails.Count == 0 && product.ServiceInDetails.Count == 0 && product.ServiceOutDetails.Count == 0)
            //            Core.Manager.ProductFacade.DeleteProduct(id);
            //        else
            //        {
            //            Response.Write("<script>alert('Delete Product Failed. Product already used in Transaction');</script>");
            //            //Message("Delete Product Failed. Product already used in Transaction");
            //            return;
            //        }
            //        BindData();

            //    }
            //}
            if (e.CommandName == "Link")
            {
                string cpdetailID = Request.QueryString["cid"];

                Control control;
                control = e.Item.FindControl("lnkCallPlanId");

                if (control != null)
                {
                    string id;
                    id = ((LinkButton)control).Text;

                    var cpDetail = CallPlanFacade.GetCallPlanByCPDetailID(cpdetailID);
                    var SameCustomer = CallPlanFacade.GetSameCustomer(cpDetail.CustomerID, id, cpDetail.WarehouseID); //001
                    if (SameCustomer != null)
                    {
                        Response.Write("<script>alert('The Customer or Warehouse is already Exist in this Call Plan');</script>"); //001
                        //Message("ID already Exist");
                        return;
                    }

                    CallPlanFacade.UpdateCPDetail(cpdetailID, id);
                    Response.Redirect("CallPlanDetail.aspx?cid=" + id);
                }
            }
        }

        //public void Message(String msg)
        //{
        //    string script = "window.onload = function(){ alert('";
        //    script += msg;
        //    script += "');";
        //    script += "window.location = '";
        //    script += "'; }";
        //    ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);
        //}

        protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            BindData(txtSearch.Text);
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;

            BindData(txtSearch.Text);
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;

            BindData(txtSearch.Text);
        }

        //private void BindDataSearch(string keyword, string keyworddate)
        //{
        //    RptCallPlanlist.DataSource = CallPlanFacade.GetSearch(keyword, keyworddate);
        //    RptCallPlanlist.DataBind();
        //}

        protected void btnSearch2_Click(object sender, EventArgs e)
        {
            lParam = "search";
            PageNumber = 0;
            BindData(txtSearch.Text);

            //BindDataSearch(txtSearch.Text, txtDate.Text);
        }

        //private void BindDataShowAll()
        //{
        //    RptCallPlanlist.DataSource = CallPlanFacade.GetCallPlanAll();
        //    RptCallPlanlist.DataBind();
        //}

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            string callPlanDetailID = Request.QueryString["cid"];
            var callPlanDetail = CallPlanFacade.GetCallPlanByCPDetailID(callPlanDetailID);

            string id;
            id = callPlanDetail.CallPlanID;

            Response.Redirect("CallPlanDetail.aspx?cid=" + id);
        }
    }
//}