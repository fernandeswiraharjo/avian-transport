﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core;
using System.Web.SessionState;

//namespace TransportService.pages
//{
public partial class SiteMaster : System.Web.UI.MasterPage
{

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (Session["User"] == "" || Session["User"] == null)
        {
            lblSesion.Text = "";
            Response.Redirect("login.aspx");
        }
        else
        {
            lblSesion.Text = Session["User"].ToString();
            var user = Core.Manager.UserFacade.GetUserbyID(lblSesion.Text);
            var listMenu = Core.Manager.UserAccessFacade.GetMenu(user.RoleID);


            rptMenu.DataSource = listMenu;
            rptMenu.DataBind();

            if (user.UserID == "admin")
            {
                int iTimeout = 1440; // 1440 minutes
                //HttpSessionState.Timeout = iTimeout;
                Session.Timeout = iTimeout;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

            //if (Session["User"] == "" || Session["User"] == null)
            //{
            //    lblSesion.Text = "";
            //    Response.Redirect("login.aspx");
            //}
            //else
            //{
            //    lblSesion.Text = Session["User"].ToString();
            //    var user = Core.Manager.UserFacade.GetUserbyID(lblSesion.Text);
            //    var listMenu = Core.Manager.UserAccessFacade.GetMenu(user.RoleID);


            //    rptMenu.DataSource = listMenu;
            //    rptMenu.DataBind();
            //}
        
    }

    protected void lnkUser_Click(object sender, EventArgs e)
    {
    }

    protected void lnklogout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Response.Redirect("login.aspx");
    }

    protected void RptMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater currentRepeater = (Repeater)sender;

            Repeater rptSubMenu = (Repeater)e.Item.FindControl("rptSubMenu");

            var data = e.Item.DataItem as Core.Model.mdlMenu;

            var listSubMenu = Core.Manager.UserAccessFacade.GetSubMenu(data.type,data.menu,data.role);

            rptSubMenu.DataSource = listSubMenu ; 
            rptSubMenu.DataBind();


        }
    }



}
//}