﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Collections;

namespace TransportService.pages
{
    public partial class CustCoordinate : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        private void ClearContent()
        {
            if (getBranch() == "")
            {
                ddlSearchBranchID.DataSource = BranchFacade.LoadBranch2(getBranch());
                ddlSearchBranchID.DataTextField = "BranchName";
                ddlSearchBranchID.DataValueField = "BranchID";
                ddlSearchBranchID.DataBind();
            }
            else
            {
                ddlSearchBranchID.DataSource = BranchFacade.LoadSomeBranch(getBranch());
                ddlSearchBranchID.DataTextField = "BranchName";
                ddlSearchBranchID.DataValueField = "BranchID";
                ddlSearchBranchID.DataBind();
            }
            rptBranchCoordinate.DataSource = ddlSearchBranchID.DataSource;
            rptBranchCoordinate.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ClearContent();
            }

        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            var listCustomerCoordinate = new List<Core.Model.mdlCustomer>();
            var listWarehouseCoordinate = new List<Core.Model.mdlWarehouse>();

                listCustomerCoordinate.AddRange(CustomerFacade.GetCustomerCoordinate(ddlSearchBranchID.SelectedValue));
                listWarehouseCoordinate.AddRange(WarehouseFacade.GetWarehouseCoordinate(ddlSearchBranchID.SelectedValue));

            if (listCustomerCoordinate.Count == 0 && listWarehouseCoordinate.Count == 0)
            {
                lblAlert.Text = "Location Unavailable";
            }
            else
            {
                lblAlert.Text = "";
                rptCustomerMarkers.DataSource = listCustomerCoordinate;
                rptWarehouseMarkers.DataSource = listWarehouseCoordinate;
                
                rptCustomerMarkers.DataBind();
                rptWarehouseMarkers.DataBind();
            }
        }

    }
}