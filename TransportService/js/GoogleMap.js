var z_index = 0;
var objMap;
var objDrag, intXPos, intYPos;
var objDummy;
var objInfoWindow;
var objIcons;

var directionsService = new google.maps.DirectionsService();
var directionsServiceTest = [];
var directionsDisplayArray = [];
var directionsDisplay ;
var intCountDirection = 0;
var mapRendererOptions = {
    markerOptions: { visible: false },
    polylineOptions: {
        strokeColor: "#0000FF",
        strokeOpacity: 4,
        strokeWeight: 2
    },
    map: ""
};

//For get Map Cordinate in screen
DummyOView.prototype = new google.maps.OverlayView();
DummyOView.prototype.draw = function () { };
document.onmouseup = function () {
    document.onmousemove = null;
    if (objDrag) { objDrag = null; }
};

//Information Window
objInfoWindow = new google.maps.InfoWindow();
objGeocoder = new google.maps.Geocoder();

//First Deploy
//fImageMarker();

var objGoogleMapOption = {
    center: new google.maps.LatLng(-6.162485, 106.843707),
    zoom: 12,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    mapTypeControlOptions: {
        mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.HYBRID]
    },
    navigationControlOptions: {
        style: google.maps.NavigationControlStyle.ZOOM_PAN
    }
};


//'<a id="lnk" href="javascript:Info('1')" onmouseover="hover.over(this.id)" onmouseout="hover.out(this.id)">' + objOutletMarkers[i].name + '<\/a>';
//var hover = { // Hovering over the links
//	over: function(id) {
//		// Set another background color for the link
//		var hovered = document.getElementById(id);
//		hovered.className = "focus";
//		
//		// Set another marker icon
//		for(var i = 0, m; m = objOutletMarkers[i]; i++) {
//			if (m.id == id) {
//				m.setIcon(icons[objOutletMarkers[i].category]);
//			}
//		}
//	},
//	out: function(id) {
//		// Set the default link background
//		var hovered = document.getElementById(id);
//		hovered.className = "normal";

//		// Set the default marker icon
//		for(var i =0, m; m = objOutletMarkers[i]; i++) {
//			if (m.id == id) {
//				m.setIcon(icons[objOutletMarkers[i].category]);
//			}
//		}
//	}
//};

var compareCats = function(a, b) {
	var n1 = a.name;
	// Treat German umlauts like non-umlauts
	n1 = n1.toLowerCase();
	n1 = n1.replace(/�/g,"a");
	n1 = n1.replace(/�/g,"o");
	n1 = n1.replace(/�/g,"u");
	n1 = n1.replace(/�/g,"s");

	var n2 = b.name;
	n2 = n2.toLowerCase();
	n2 = n2.replace(/�/g,"a");
	n2 = n2.replace(/�/g,"o");
	n2 = n2.replace(/�/g,"u");
	n2 = n2.replace(/�/g,"s");
	var c1 = a.category;
	var c2 = b.category;

	// Sort categories and names
	if(a.category == b.category){
		if(a.name == b.name){
			return 0;
		}
		return (a.name < b.name) ? -1 : 1;
	}
	return (a.category < b.category) ? -1 : 1;
}

//function fImageMarker() {
//    $.ajax({
//        type: "POST",
//        url: 'WebService/GoogleWebData.asmx/GetImageMarker',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (data, status) {
//            objIcons = JSON.parse(data.d);
//        },
//        error: function (request, status, error) {
//            alert("Error : " + request.statusText);
//        }
//    });
//}

//function fCreateMarker(intLatLng, strId, strName, strInfoWindow, strType, strCategory, objArrMarker, strNumber) {
//    var g = google.maps;
//    var objShadow = null;

//    if (strId.substr(0, 4) != "Hid_" && strId.substr(0, 4) != "Oth_" && strCategory != "Track") {
//        if (strCategory == "Trans") {
//            if (strNumber == "") {
//                var objImage = new g.MarkerImage(objIcons.Gimg, new g.Size(32, 32), new g.Point(0, 0), new g.Point(16, 32));
//            } else {
//                var objImage = new g.MarkerImage('https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + strNumber, new g.Size(32, 32), new g.Point(0, 0), new g.Point(16, 32));
//            }
//        } else {
//            var objImage = new g.MarkerImage(objIcons[strCategory], new g.Size(32, 32), new g.Point(0, 0), new g.Point(16, 32));
//            var objShadow = new g.MarkerImage(objIcons.Gimg, new g.Size(32, 32), new g.Point(192, 32), new g.Point(16, 32));
//        }
//    } else {
//        var objImage = new g.MarkerImage(objIcons[strCategory], new g.Size(6, 6), new g.Point(0, 0), new g.Point(3, 3));
//	}

//	var objMarker = new g.Marker({ position: intLatLng, map: objMap,
//	    title: strName, clickable: true, draggable: false, center: intLatLng,
//	    icon: objImage, shadow: objShadow
//	});

//	// Store category, name, and id as marker properties
//	objMarker.type = strType;
//	objMarker.category = strCategory;
//	objMarker.name = strName;
//	objMarker.id = strId;

//    //Saving To Array
//	objArrMarker.push(objMarker);

//	g.event.addListener(objMarker, "click", function () {
//	    actual = objMarker;
//	    var lat = actual.getPosition().lat();
//	    var lng = actual.getPosition().lng();
//	    strInfoWindow = "<div style='padding:10px; background:#fff;'><table><table cellpadding='2' cellspacing='2' width='100%'><tr><td>" + strInfoWindow + "</td></tr></div>";
//	    objInfoWindow.setContent(strInfoWindow);
//	    objInfoWindow.open(objMap, this);
//	});
//}

function fSearchAddress(strAddress){
	with (document.forms[0]){
	    objGeocoder.geocode({ 'address': strAddress }, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
			    objMap.setCenter(results[0].geometry.location);
			}
		});
	}
}

function fclearOverlays(objArrMarker) {
    if (objArrMarker.length > 0) {
        for (var i = 0; i < objArrMarker.length; i++) {
            objArrMarker[i].setMap(null);
		}
        objArrMarker = []
	}
}

function fOpenInfoWindow(strId, objArrMarker) {
    for (var i = 0, m; m = objArrMarker[i]; i++) {
        if (m.id == strId) {
            google.maps.event.trigger(objArrMarker[i], "click");
            objMap.panTo(intLatLng);

            break;
        }
    }
    if (objArrMarker.length > 0) {
        if (objArrMarker.length >= strId) {
            google.maps.event.trigger(objArrMarker[strId], "click");
        }
    }
}

function fGetRoutingLine(objRoute) {
    var waypts = [];
    var objrequest = [];
    var request = {};

    var intRoute = 0;
    var distance;
    var objLastPlace;
    var objNextPlace;

    var objOrigin;
    var objDestination;
    var objWayPoint = [];

    //now set renderer
    objOrigin = m_strDistroLoc
    objDestination = m_strDistroLoc;
    objNextPlace = m_strDistroLoc
    objLastPlace = m_strDistroLoc;

    var intDirection = 0;
    var intService = 0;
    var intMaxDirection = 8;

    //Redefine Direction Display
    intCountDirection = 0;
    for (var q = 0; q < directionsDisplayArray.length; q++) {
        directionsDisplayArray[q].setMap(null);
        directionsDisplayArray[q].setMap(objMap);
    }

    for (var i = 0; i < objRoute.length; i++) {
        if (intRoute < intMaxDirection) {
            //Way Point
            waypts.push({ location: objRoute[i], stopover: true });
            intRoute++;

            if (i == objRoute.length - 1) {
                if (objDestination != "") {
                    objLastPlace = objDestination;
                } else {
                    objLastPlace = objRoute[i];
                }

                //Insert Into Directions Service
                request = {
                    origin: objNextPlace,
                    destination: objLastPlace,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING,
                    waypoints: waypts,
                    optimizeWaypoints: false,
                    avoidHighways: true,
                    avoidTolls: true
                };
                objrequest.push(request);

                directionsService = new google.maps.DirectionsService();
                directionsServiceTest.push(directionsService);

                intService++;
            }
        } else if (intRoute == intMaxDirection) {
            if (intService == 0) {
                objNextPlace = objOrigin;
            } else {
                objNextPlace = objLastPlace;
            }
            objLastPlace = objRoute[i];

            //Insert Into Directions Service
            request = {
                origin: objNextPlace,
                destination: objLastPlace,
                travelMode: google.maps.DirectionsTravelMode.DRIVING,
                waypoints: waypts,
                optimizeWaypoints: false,
                avoidHighways: true,
                avoidTolls: true
            };

            //Saving To Array
            objrequest.push(request);
            directionsService = new google.maps.DirectionsService();
            directionsServiceTest.push(directionsService);

            waypts = [];
            waypts.length = 0;

            intRoute = 0;
            intService++;
            intRoute++;
        }
    }


    //Create Routing Line
    for (var q = 0; q < intService; q++) {
        if (typeof (directionsDisplayArray[q]) == 'undefined') {
            directionsDisplay = new google.maps.DirectionsRenderer();
            directionsDisplay.setMap(objMap);
            directionsDisplay.setOptions(new google.maps.DirectionsRenderer(mapRendererOptions));
            directionsDisplayArray.push(directionsDisplay);
        }

        directionsServiceTest[q].route(objrequest[q], function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplayArray[intCountDirection].setDirections(response);
                intCountDirection++;
            }
        });
    }
    
}

//-------------------------- Create Dragable Image to get GMap Position --------------------------//
function DummyOView(objMap) {
    this.setMap(objMap);
}

function initDrag(e) {
    if (!e) var e = window.event;
    // Drag image's parent div element
    objDrag = e.target ? e.target.parentNode : e.srcElement.parentElement;
    if (objDrag.className.substr(0, 4) != "drag") {
        if (e.cancelable) e.preventDefault();
        objDrag = null;
        return;
    }

    if (objDrag) {
        // The currently dragged object always gets the highest z-index
        z_index++;
        objDrag.style.zIndex = z_index.toString();

        intXPos = e.clientX - objDrag.offsetLeft;
        intYPos = e.clientY - objDrag.offsetTop;

        document.onmousemove = moveObj;
    }
    return false;
}

function moveObj(e) {
    if (objDrag && objDrag.className.substr(0, 4) == "drag") {
        if (!e) var e = window.event;
        objDrag.style.left = e.clientX - intXPos + "px";
        objDrag.style.top = e.clientY - intYPos + "px";

        objDrag.onmouseup = function () {
            var gd = objMap.getDiv();
            var mLeft = gd.offsetLeft;
            var mTop = gd.offsetTop;

            var mWidth = gd.offsetWidth;
            var mHeight = gd.offsetHeight;

            var areaLeft;
            var areaTop;

            if (objDrag.className == "drag") {
                areaLeft = drag_area.offsetLeft;
                areaTop = drag_area.offsetTop;
            } else if (objDrag.className == "drag2") {
                areaLeft = drag_area2.offsetLeft;
                areaTop = drag_area2.offsetTop;
            } else {
                areaLeft = drag_area3.offsetLeft;
                areaTop = drag_area3.offsetTop;
            }

            var oWidth = objDrag.offsetWidth;
            var oHeight = objDrag.offsetHeight;

            // The object's pixel position relative to the document
            var x = objDrag.offsetLeft + areaLeft + (oWidth / 2);
            var y = objDrag.offsetTop + areaTop + (oHeight / 2);

            // Check if the cursor is inside the map div
            if (x > mLeft && x < (mLeft + mWidth) && y > mTop && y < (mTop + mHeight)) {
                // Difference between the x property of iconAnchor
                // and the middle of the icon width
                var anchorDiff = 1;

                // Find the object's pixel position in the map container
                var g = google.maps;
                var pixelpoint = new g.Point(x - mLeft - anchorDiff, y - mTop + (oHeight / 2));

                // Corresponding geo point on the map
                var proj = objDummy.getProjection();
                var latlng = proj.fromContainerPixelToLatLng(pixelpoint);


                fDragImageSuccess(latlng, objDrag.className);
            }
        }
    }
    return false;
}

function fillMarker(strClassName) {
    var m = document.createElement("div");
    m.style.position = "absolute";
    m.style.width = "32px";
    m.style.height = "32px";
    m.style.left = "0px";

    // Set the same id and class attributes again
    m.id = objDrag.id;
    m.className = strClassName;

    // Append icon
    var img = document.createElement("img");
    img.src = objDrag.firstChild.getAttribute("src");
    img.style.width = "32px";
    img.style.height = "32px";
    m.appendChild(img);

    if (strClassName == "drag") {
        drag_area.replaceChild(m, objDrag);
    } else if (strClassName == "drag") {
        drag_area2.replaceChild(m, objDrag);
    } else {
        drag_area3.replaceChild(m, objDrag);
    }

    // Clear initial object
    objDrag = null;
}