﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core.Model
{
    [DataContract]
    public class mdlResult
    {
        [DataMember]
        public string Result { get; set; }

    }

    [DataContract]
    public class mdlResultList
    {
        [DataMember]
        public List<mdlResult> ResultList { get; set; }
    }
}
