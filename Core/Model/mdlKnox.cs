﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core.Model
{

    public class mdlKnox
    {
        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string KnoxKey { get; set; }
    }

    public class mdlKnoxParam
    {
        [DataMember]
        public string Password { get; set; }
    }

}
