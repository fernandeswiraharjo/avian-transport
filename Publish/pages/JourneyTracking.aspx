﻿<%@ Page Title="Journey Tracking" Language="C#" MasterPageFile="~/pages/Site.Master"
    AutoEventWireup="true" CodeBehind="JourneyTracking.aspx.cs" Inherits="TransportService.pages.JourneyTracking" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form Visit Tracking</h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Form Basic Elements <small>different form elements</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a> </li>
                            <li><a href="#">Settings 2</a> </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <br />
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <asp:ScriptManager ID="ScriptManager2" runat="server">
                                    </asp:ScriptManager>
                                    <p>
                                        <asp:Label ID="lblBranchID" runat="server" Text="Branch ID" Width="100px"></asp:Label>
                                        <asp:Label ID="Label2" runat="server" Text=":" Width="10px"></asp:Label>
                                        <asp:TextBox ID="txtBranchID" runat="server" Style="margin-left: 9px; margin-top: 0px"
                                            Width="150px" CssClass="tb5"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtBranchID" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        <ajaxToolkit:AutoCompleteExtender ID="txtBranchID_AutoCompleteExtender" runat="server"
                                            ServiceMethod="SearchBranchID" MinimumPrefixLength="2" EnableCaching="false"
                                            DelimiterCharacters="" Enabled="True" TargetControlID="txtBranchID">
                                        </ajaxToolkit:AutoCompleteExtender>
                                    </p>
                                    <p>
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee ID" Width="100px"></asp:Label>
                                        <asp:Label ID="Label4" runat="server" Text=":" Width="10px"></asp:Label>
                                        <asp:TextBox ID="txtEmployee" runat="server" Style="margin-left: 9px; margin-top: 0px"
                                            Width="150px" CssClass="tb5"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtEmployee" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" ServiceMethod="SearchEmployee"
                                            MinimumPrefixLength="0" EnableCaching="false" DelimiterCharacters="" Enabled="True"
                                            TargetControlID="txtEmployee">
                                        </ajaxToolkit:AutoCompleteExtender>
                                    </p>
                                    <p>
                                        <asp:Label ID="lblDate" runat="server" Text="Date" Width="100px"></asp:Label>
                                        <asp:Label ID="Label3" runat="server" Text=":" Width="10px"></asp:Label>
                                        <asp:TextBox ID="txtDate" runat="server" Style="margin-left: 9px; margin-top: 0px"
                                            Width="150px" CssClass="tb5"></asp:TextBox>
                                        <script type="text/javascript">
                                            var picker1 = new Pikaday(
                                                        {
                                                            field: document.getElementById('<%=txtDate.ClientID%>'),
                                                            firstday: 1,
                                                            minDate: new Date('2000-01-01'),
                                                            maxDate: new Date('2020-12-31'),
                                                            yearRange: [2000, 2020]
                                                            //                                    setDefaultDate: true,
                                                            //                                    defaultDate : new Date()
                                                        }
                                                        );
                                        </script>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtDate" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            
                                    </p>
                                    <asp:Button ID="btnShow" runat="server" Text="Show Tracking" class="btn btn-sm btn-embossed btn-primary"
                                        Font-Size="14px" OnClick="btnShow_Click" ValidationGroup="valShowReport" />
                                </div>
                            </div>
                        </div>
                                        </div>
            </div>
        </div>
    <%-- SHOW MAP--%>
    <div class="col-md-12 col-xs-12">
                <asp:Label ID="lblAlert" runat="server" Text="" Style="text-align: center; color: #FF0000;
                    font-weight: 700"></asp:Label>
                <div id="dvMap" style="width: 100%; height: 640px">
                </div>
            </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
    <link href="../js/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4a8ijWorZUkJKOtzheGKtV1RlyfDYW24&callback=initMap"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <%-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>--%>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../js/FormatNumber.js" type="text/javascript"></script>
    <script src="../js/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <%-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry,places"></script>--%>
    <!-- Support Jquery -->
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script src="../js/FormatNumber.js" type="text/javascript"></script>
    <script src="../js/common.js" type="text/javascript"></script>
    <script src="../js/MapDefault.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/GoogleMap.js"></script>
    <script src="../js/mapwithmarker.js" type="text/javascript"></script>
    <style type="text/css">
        .labels
        {
            color: black;
            background-color: #FF8075;
            font-family: Arial;
            font-size: 11px;
            font-weight: bold;
            text-align: center;
            width: 25px;
        }
    </style>
    <script type="text/javascript">
        function loadMap() {

            //Create Map
            objMap = new google.maps.Map(document.getElementById("dvMap"), objGoogleMapOption);

        }

        $(function () {
            loadMap();
        });
    </script>
    <script type="text/javascript">
        var markers = [
        <asp:Repeater ID="rptJourneyMarkers" runat="server">
        <ItemTemplate>
                    {
                    "title": '<%# Eval("TrackingDate") %>',
                    "lat": '<%# Eval("Latitude") %>',
                    "lng": '<%# Eval("Longitude") %>',
                    "description": '<%# Eval("TrackingDate") %>',
                    "lStreetName": '<%# Eval("StreetName") %>'
                }
        </ItemTemplate>
        <SeparatorTemplate>
            ,
        </SeparatorTemplate>
        </asp:Repeater>
        ];
    </script>
    <script type="text/javascript">

        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var infowindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);

            var lat_lng = new Array();
            for (i = 0; i < markers.length; i++) {
                var data = markers[i];
                var start = new google.maps.LatLng(data.lat, data.lng);
                var contentString = data.lStreetName + ' - ' + data.description;

                lat_lng.push(start);

                var marker1 = new MarkerWithLabel({
                    position: start,
                    map: map,
                    title: data.title,
                    labelContent: i,
                    labelAnchor: new google.maps.Point(15, 65),
                    labelClass: "labels", // the CSS class for the label
                    labelInBackground: false,
                    icon: "../Images/Visit.png"
                });
                google.maps.event.addListener(marker1, 'click', (function (marker1, contentString, infowindow) {
                    return function () {
                        infowindow.setContent(contentString);
                        infowindow.open(map, marker1);
                    };
                })(marker1, contentString, infowindow));
            }
        }
            
    </script>
</asp:Content>
