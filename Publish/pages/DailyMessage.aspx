﻿<%@ Page Title="Daily Message" Language="C#" MasterPageFile="~/pages/Site.Master"
    AutoEventWireup="true" CodeBehind="DailyMessage.aspx.cs" Inherits="TransportService.pages.DailyMessage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form Daily Message</h3>
        </div>
        
       <%-- <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            Go!</button>
                    </span>
                </div>
            </div>
        </div>--%>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
           <%-- <div class="x_title">
                <h2>
                    Form Basic Elements <small>different form elements</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a> </li>
                            <li><a href="#">Settings 2</a> </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>--%>
            <div class="x_content">
                <br />
                <div class="form-group">

                 <asp:Label ID="lbl1" runat="server" Text="Message ID or Name :" Width="160px" 
                        Cssclass="control-label col-md-3 col-sm-3 col-xs-12" 
                        Style="margin-top: 5px;" Font-Bold="True"></asp:Label>
                                <asp:TextBox ID="txtSearch" runat="server" Style="margin-left: -10px; margin-top: 0px"
                                    Width="200px" CssClass="form-control col-md-3 col-xs-12"></asp:TextBox>

                                    <asp:Label ID="lbl2" runat="server" Text="Valid Date :" Width="100px"  Cssclass="control-label col-md-3 col-sm-3 col-xs-12" 
                        Style="margin-top: 5px; margin-left:10px" Font-Bold="True"></asp:Label>
                        <asp:TextBox ID="txtSearch2" runat="server" Style="margin-left: -10px; margin-top: 0px"
                        Width="200px" CssClass="form-control col-md-3 col-xs-12"></asp:TextBox>
                    <script type="text/javascript">
                        var picker1 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=txtSearch2.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020]
                                                     //                                    setDefaultDate: true,
                                                     //                                    defaultDate : new Date()
                                                 }
                                                                          );
                    </script>
                    <asp:Button ID="btnSearch2" runat="server" Text="Search" OnClick="btnSearch2_Click"
                        class="btn btn-primary" Style="margin-left: 10px;"></asp:Button>
                                    
                                    <%--CssClass="tb5"--%>

                    <%--</div>--%>
                    <br />
                    <br />
                    <div class="datagrid" style="width: 1000px">
                        <table style="width: 100%; margin-bottom: 0px;">
                            <thead>
                                <tr>
                                    <th>
                                        Message ID
                                    </th>
                                    <th>
                                        Message Name
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        End Date
                                    </th>
                                    <th>
                                        Created By
                                    </th>
                                    <th>
                                    </th>
                                    <th>
                                    </th>
                                    <th>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="RptDailyMsglist" runat="server" OnItemCommand="rptDailyMsg_ItemCommand">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="MsgID" runat="server" Text='<%# Eval("MessageID") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="MsgNm" runat="server" Text='<%# Eval("MessageName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="Date" runat="server" Text='<%# Eval("Date") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="EndDate" runat="server" Text='<%# Eval("EndDate") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="CreatedBy" runat="server" Text='<%# Eval("CreatedBy") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkUpdate" Text="View/Update" runat="server" Visible='<%# Eval("Role") %>' CommandName="Update" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkDelete" Text="Delete" Visible='<%# Eval("Role") %>' OnClientClick="return confirm('Are you sure you want to Delete this Daily Message ?');"
                                                    runat="server" CommandName="Delete" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkMngRole" Text="Role" Visible='<%# Eval("Role") %>' OnClientClick="return confirm('Are you sure you want to Manage this Daily Message's Access Role ?');"
                                                    runat="server" CommandName="Role" />
                                            </td>
                                        </tr>
                                        </tbody>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tfoot>
                                    <tr>
                                        <td colspan="8">
                                            <div id="paging">
                                                <ul>
                                                    <li>
                                                        <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span>Previous</span></asp:LinkButton></li>
                                                    <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                    runat="server"><span><%# Container.DataItem %></span></asp:LinkButton>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <li>
                                                        <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span>Next</span></asp:LinkButton></li>
                                                </ul>
                                            </div>
                                    </tr>
                                </tfoot>
                        </table>
                    </div>
                    <br />
                    <asp:Button ID="btnAdd" runat="server" Text="Add New" class="btn btn-primary" OnClick="btnAdd_Click" />
                    <%-------------------------------------------Form Input------------------------------------------------%>
               </div>
            </div>
        </div>
    </div>

    <asp:Panel ID="PanelFormDailyMsg" runat="server" Width="100%">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>
                                    Form Daily Message Management </h2>
                                <%--<ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a> </li>
                                            <li><a href="#">Settings 2</a> </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                </ul>--%>
                                <div class="clearfix">
                                </div>
                            </div>
                            <div class="x_content">
                                <br />
                                <contenttemplate>
                    <div>
                        <fieldset>
                            <p>
                                <asp:Label ID="lblMsgID" runat="server" Text="Message ID" Font-Bold="True"></asp:Label><br />
                                <asp:TextBox ID="txtMsgID" runat="server" Width="400px" CssClass="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" 
                                          ControlToValidate="txtMsgID" ValidationGroup="DailyMsg" ForeColor="#FF3300" 
                                          SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </p>
                            <p>
                                <asp:Label ID="lblMsgNm" runat="server" Text="Message Name" Font-Bold="True"></asp:Label><br />
                                <asp:TextBox ID="txtMsgNm" runat="server" Width="400px" CssClass="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" 
                                          ControlToValidate="txtMsgNm" ValidationGroup="DailyMsg" ForeColor="#FF3300" 
                                          SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </p>
                            
                            <%--<asp:Panel ID="Panel1" CssClass="form-control" Height="400px" Width="400px" runat="server" ScrollBars="Vertical">
                                        <asp:CheckBoxList ID="blBranch" runat="server">
                                        </asp:CheckBoxList>
                                        </asp:Panel>
                                        <asp:Button ID="btnSelectAll" runat="server" CssClass="btn btn-sm " Text="SELECT ALL" onclick="btnSelectAll_Click" />
                                         <asp:Button ID="btnUnSelectAll" runat="server" CssClass="btn btn-sm " Text="UNSELECT ALL" onclick="btnUnSelectAll_Click" />--%>
                  

                                <p>
                                    <asp:Label ID="lblMsgDesc" runat="server" Text="Message Description" Font-Bold="True"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtMsgDesc" runat="server" Width="600px" Height="300px" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" 
                                          ControlToValidate="txtMsgDesc" ValidationGroup="DailyMsg" ForeColor="#FF3300" 
                                          SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </p>
                                <p>
                                    <asp:Label ID="lblMsgImg" runat="server" Text="Image" Font-Bold="True"></asp:Label>
                                    <br />
                                    <asp:FileUpload ID="FileUpload1" runat="server"></asp:FileUpload>
                                    <br />
                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                     
                                    <asp:Image ID="Image1" Visible="false" runat="server" height="200" width="200"></asp:Image>
                                    &nbsp;<asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btn btn-success" onclick="btnUpload_Click"/><%--CssClass="btn btn-sm"--%>
                                    <br />
                                    <%--<br />--%>
                                    <asp:TextBox ID="txtMsgImg" runat="server" Width="800px" CssClass="tb5" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtMsgImgPath" runat="server" Width="800px" CssClass="tb5" Visible="false"></asp:TextBox>
                                    <%--<br />--%>
                                    <%--<asp:Label ID="lblStatus" runat="server" style="color:Red;"></asp:Label>--%>
                                </p>
                                <p>
                                    <asp:Label ID="lblDate" runat="server" Text="Date" Font-Bold="True"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtDate" runat="server" Width="400px" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" 
                                          ControlToValidate="txtDate" ValidationGroup="DailyMsg" ForeColor="#FF3300" 
                                          SetFocusOnError="True"></asp:RequiredFieldValidator>

                                          <script type="text/javascript">
                                              var picker2 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=txtDate.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020]
                                                     //                                    setDefaultDate: true,
                                                     //                                    defaultDate : new Date()
                                                 }
                                                                          );
                                               </script>
                                </p>
                                <p>
                                    <asp:Label ID="lblEndDate" runat="server" Text="End Date" Font-Bold="True"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtEndDate" runat="server" Width="400px" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" 
                                          ControlToValidate="txtEndDate" ValidationGroup="DailyMsg" ForeColor="#FF3300" 
                                          SetFocusOnError="True"></asp:RequiredFieldValidator>

                                          <script type="text/javascript">
                                              var picker3 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=txtEndDate.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020]
                                                     //                                    setDefaultDate: true,
                                                     //                                    defaultDate : new Date()
                                                 }
                                                                          );
                                               </script>
                                </p>
                                 <p>
                                    <asp:Label ID="lblMsgCreator" runat="server" Text="Created By" Font-Bold="True"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtMsgCreator" runat="server" Width="400px" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*" 
                                          ControlToValidate="txtMsgCreator" ValidationGroup="DailyMsg" ForeColor="#FF3300" 
                                          SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </p>
                                <br />
                                <p>
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" onclick="btnUpdate_Click" ValidationGroup="DailyMsg"
                                />
                                <asp:Button ID="btnInsert" runat="server" Text="Insert" class="btn btn-primary" onclick="btnInsert_Click" ValidationGroup="DailyMsg"
                                />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-primary" onclick="btnCancel_Click"
                                />
                                </p>
                        </fieldset>
                        </div>
                            </div>
                        </div>
                    </div>

    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
